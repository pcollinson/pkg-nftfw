% nftfw document index
# _nftfw_ document index
- [User's Guide](Users_Guide.md)
  - The full User guide, the first section explains how the system is controlled.
- [How do I.. or Quick User's Guide](How_do_I.md)
  - Answers a bunch of questions about the system.
- [Install _nftfw_ from Debian package](Debian_package_install.md)
  - Installation from the package found in the package directory
- [Installing _nftfw manually_](Installation.md)
  - Full installation of the system for Debian Buster.
- [Manual Installation Instructions](Installation-Instructions.md)
  - For those who want a bare bones list of tasks.
- [Installing Geolocation](Installing-GeoLocation.md)
  - Installing Geolocation, adding country detection to _nftfwls_, which is optional but desirable.
- [Getting CIDR lists](Getting-cidr-lists.md)
  - How to get CIDR files for use with the _blacknet_ feature.
- [Updating _nftfw_](Updating-nftfw.md)
  - How to update a manual installation of_nftfw_.
